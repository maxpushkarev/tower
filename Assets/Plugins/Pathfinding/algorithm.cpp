﻿#include <queue>
#include <vector>

#ifdef _WINDOWS
#define EXPORT __declspec (dllexport)
#else 
#define EXPORT
#endif
typedef unsigned char CELL_DATA;

class Direction
{
public:
	const int Dx;
	const int Dy;
	Direction(const int dx, const int dy) : Dx(dx), Dy(dy)
	{
	}
	~Direction()
	{
	}
};

class Node
{
public:
	const int CellIndex;
	const int DirectionIndex;
	Node(const int cellIndex, const int dirIdx) : CellIndex(cellIndex), DirectionIndex(dirIdx)
	{
	}
	~Node()
	{
	}
};

class Solver
{
private:
	const int _size;
	std::vector<int>* _visitedVertices;
	std::queue<Node>* _bfs;
	const int _reverseDirMap[4] = { 1, 0, 3, 2 };
	const Direction _directions[4] = {Direction(0,1), Direction(0,-1), Direction(1,0), Direction(-1,0) };

	void Clear(CELL_DATA* data);
	bool IsObstacle(const CELL_DATA* data, const int idx);
	void SetDirectionIndex(CELL_DATA* data, const int idx, const int value);

public:
	Solver(const int size);
	~Solver();
	void Solve(CELL_DATA* data, const int target);
};

static Solver* _solver;

extern "C"
{
	EXPORT void Init(const int size)
	{
		_solver = new Solver(size);
	}

	EXPORT void RunBfs(CELL_DATA* data, const int target)
	{
		_solver->Solve(data, target);
	}

	EXPORT void DisposeNative()
	{
		delete(_solver);
	}
}


Solver::Solver(const int size) : _size(size)
{
	_visitedVertices = new std::vector<int>();
	_bfs = new std::queue<Node>();
	
	auto count = _size * _size;
	for (auto i = 0; i < count; i++)
	{
		_visitedVertices->push_back(0);
	}
}

Solver::~Solver()
{
	delete(_visitedVertices);
	delete(_bfs);
}

void Solver::Solve(CELL_DATA* data, const int target)
{
	Clear(data);

	_bfs->push(Node(target, -1));
	_visitedVertices->at(target) = 1;

	while (!_bfs->empty())
	{
		auto curr = _bfs->front();
		_bfs->pop();


		SetDirectionIndex(data, curr.CellIndex, curr.DirectionIndex < 0 ? curr.DirectionIndex : _reverseDirMap[curr.DirectionIndex]);
		auto x = curr.CellIndex / _size;
		auto y = curr.CellIndex % _size;

		for (auto i = 0; i < 4; i++)
		{
			auto dir = _directions[i];
			auto newX = x + dir.Dx;
			auto newY = y + dir.Dy;
			auto newLin = newX * _size + newY;

			if (newX >= 0 && newY >= 0 && newX < _size && newY < _size && _visitedVertices->at(newLin) == 0 && !IsObstacle(data, newLin))
			{
				_visitedVertices->at(newLin) = 1;
				_bfs->push(Node(newLin, i));
			}
		}
	}

}

void Solver::Clear(CELL_DATA* data)
{
	std::fill(_visitedVertices->begin(), _visitedVertices->end(), 0);

	auto count = _size * _size;
	for (auto i = 0; i < count; i++)
	{
		SetDirectionIndex(data, i, -1);
	}
}

void Solver::SetDirectionIndex(CELL_DATA* data, const int cell, const int value)
{
	int actualValue = value + 1;
	int curr = data[cell];
	curr &= ~7;
	curr |= actualValue;
	*(data + cell) = (CELL_DATA)curr;
}

bool Solver::IsObstacle(const CELL_DATA* data, const int cell)
{
	int curr = *(data + cell);
	return (curr & (1 << 3)) > 0;
}
