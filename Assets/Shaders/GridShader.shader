﻿Shader "Grid"
{
		Properties
		{
			[PerRendererData] [HideInInspector] _MainTex("Main Tex", 2D) = "white"{}
			_GridSize("GridSize", int) = 32
			_ActualCellSizeFactor("Actual Cell Size Factor", Range(0.0, 1.0)) = 1.0
		}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Cull Off
			Lighting Off
			ZWrite Off
			ZTest[unity_GUIZTestMode]
			Blend Off
			ColorMask rgb

			Pass
			{
				Name "Default"
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0

				#include "UnityCG.cginc"
				#include "UnityUI.cginc"

				uniform int _GridSize;
				uniform half _ActualCellSizeFactor;
				uniform sampler2D _Map;
				sampler2D _MainTex;

				uniform half4 _TargetColor, _FreeColor, _ObstacleColor;

				struct appdata_t
				{
					half4 vertex   : POSITION;
					half4 color    : COLOR;
					half2 uv : TEXCOORD0;
				};

				struct v2f
				{
					half4 vertex   : SV_POSITION;
					half4 color : COLOR;
					half2 uv : TEXCOORD0;
				};

				v2f vert(appdata_t v)
				{
					v2f OUT;
					OUT.vertex = UnityObjectToClipPos(v.vertex);
					OUT.color = v.color;
					OUT.uv = v.uv;

					return OUT;
				}

				half4 frag(v2f IN) : SV_Target
				{
					half borderColor = IN.color;
					half2 uv = IN.uv;


					half fullCellSize = 1.0h / _GridSize;
					half cellWithoutBorderSize = fullCellSize * _ActualCellSizeFactor;
					half4 halfBorderSize = (fullCellSize - cellWithoutBorderSize) * 0.5h;

					int2 coord = floor(uv / fullCellSize);
					uv -= coord * half2(fullCellSize, fullCellSize);
	


					bool XAsCell = (uv.x > halfBorderSize && uv.x < fullCellSize - halfBorderSize);
					bool YAsCell = (uv.y > halfBorderSize&& uv.y < fullCellSize - halfBorderSize);
					
					half isCell = lerp(
						0.0h, 
						1.0h,
						(XAsCell && YAsCell)
					);

					coord.xy = coord.yx;

					half len = 1.0h / _GridSize;
					half2 mapCoord = coord * len + 0.5h * len;
					half colorFactor = tex2D(_Map, mapCoord);
					half4 cellColor = _TargetColor;
					cellColor = lerp(cellColor, _FreeColor, colorFactor > 0.01h);
					cellColor = lerp(cellColor, _ObstacleColor, colorFactor > 0.51h);

					half4 res = lerp(borderColor, cellColor, isCell);

					return res;
				}
			ENDCG
			}
		}
}