﻿Shader "Unit"
{
		Properties
		{
			_MainTex("Main Tex", 2D) = "white" {}
			_Tint("Tint", Color) = (0.0,0.0,0.0,0.0)
		}

		SubShader
		{
			Tags
			{
				"DisableBatching" = "True"
				"Queue" = "Geometry"
				"IgnoreProjector" = "True"
				"PreviewType" = "Plane"
			}

			Cull Off
			Lighting Off
			ZWrite Off
			ZTest Always
			Blend SrcAlpha OneMinusSrcAlpha
			ColorMask rgb

			Pass
			{
				Name "Simple"
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				#pragma multi_compile_instancing

				#include "UnityCG.cginc"

				sampler2D _MainTex;
				half4 _Tint;

				struct appdata_t
				{
					half4 vertex   : POSITION;
					half2 uv : TEXCOORD0;
					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				struct v2f
				{
					half4 vertex   : SV_POSITION;
					half2 uv : TEXCOORD0;
				};


				v2f vert(appdata_t v)
				{
					v2f OUT;
					UNITY_SETUP_INSTANCE_ID(v);
					OUT.vertex = UnityObjectToClipPos(v.vertex);
					OUT.uv = v.uv;
					return OUT;
				}

				half4 frag(v2f IN) : SV_Target
				{
					half4 res = tex2D(_MainTex, IN.uv);
					res *= _Tint;
					return res;
				}
			ENDCG
			}
		}
}