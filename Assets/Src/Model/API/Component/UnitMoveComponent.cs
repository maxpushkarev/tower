﻿using Unity.Entities;

namespace Tower.Models
{
    public struct UnitMoveComponent : IComponentData
    {
        public int From;
        public float CurrentTime;
        public float InitialTime;
    }
}