﻿namespace Tower.Models
{
    public interface IReadonlyGridModelProvider
    {
        IReadOnlyGridModel Model { get; }
    }
}