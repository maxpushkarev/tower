﻿using Unity.Collections;

namespace Tower.Models
{
    public interface IReadOnlyGridModel
    {
        bool IsObstacle(int cell);
        int GetDirectionIndex(int cell);
        NativeGridModel Clone(
            Allocator allocator = Allocator.TempJob, 
            NativeArrayOptions opts = NativeArrayOptions.UninitializedMemory);
    }
}
