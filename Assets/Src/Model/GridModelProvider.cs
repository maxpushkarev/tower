﻿namespace Tower.Models
{
    public class GridModelProvider : IReadonlyGridModelProvider
    {
        private IReadOnlyGridModel _model;
        public IReadOnlyGridModel Model => _model;

        public void Publish(IReadOnlyGridModel model) => _model = model;
    }
}