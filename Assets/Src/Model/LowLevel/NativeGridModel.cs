﻿using System;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Tower.Models
{
    public unsafe struct NativeGridModel : IDisposable, IEquatable<NativeGridModel>
    {
        private NativeArray<byte> _rawData;

        public NativeGridModel(
            int size,
            Allocator allocator =  Allocator.Persistent, 
            NativeArrayOptions opts = NativeArrayOptions.ClearMemory
            )
        {
            _rawData = new NativeArray<byte>(
                size * size,
                allocator,
                opts);
        }

        public bool IsObstacle(int cell) => AsReadOnly().IsObstacle(cell);
        public int GetDirectionIndex(int cell) => AsReadOnly().GetDirectionIndex(cell);

        public void SetObstacle(int cell, bool isObstacle)
        {
            if (IsObstacle(cell) != isObstacle)
            {
                int curr = _rawData[cell];
                curr ^= (1 << 3);
                _rawData[cell] = (byte)curr;
            }
        }

        public void SetDirectionIndex(int cell, int dirIndex)
        {
            dirIndex++;
            int curr = _rawData[cell];
            curr &= ~7;
            curr |= dirIndex;
            _rawData[cell] = (byte)curr;
        }

        public void CopyTo(NativeGridModel target) => _rawData.CopyTo(target._rawData);
        public void* UnsafePointer => _rawData.GetUnsafePtr();
        public void Dispose() => _rawData.Dispose();

        public override bool Equals(object obj) => Equals((NativeGridModel) obj);

        public override int GetHashCode() => _rawData.GetHashCode();

        public NativeReadonlyGridModel AsReadOnly() => new NativeReadonlyGridModel(this);

        public bool Equals(NativeGridModel other)
        {
            return UnsafeUtility.MemCmp(
                UnsafePointer,
                other.UnsafePointer,
                UnsafeUtility.SizeOf<byte>() * _rawData.Length
            ) == 0;
        }


        public struct NativeReadonlyGridModel
        {
            private NativeArray<byte>.ReadOnly _rawData;

            public NativeReadonlyGridModel(NativeGridModel readWriteModel)
            {
                _rawData = readWriteModel._rawData.AsReadOnly();
            }

            public bool IsObstacle(int cell) => (((int)_rawData[cell]) & (1 << 3)) > 0;
            public int GetDirectionIndex(int cell) => (((int)_rawData[cell]) & 7) - 1;
        }

    }
}