﻿using System;
using Unity.Collections;

namespace Tower.Models
{
    public interface IGridModel : IReadOnlyGridModel, IDisposable
    {
        int Version { get; set; }
        void SetObstacle(int cell, bool isObstacle);
        void SetDirectionIndex(int cell, int dirIndex);
        void CopyTo(IGridModel target);
        NativeGridModel GridRawData { get; }
    }
}