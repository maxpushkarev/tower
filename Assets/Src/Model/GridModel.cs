﻿using Tower.Config;
using Unity.Collections;

namespace Tower.Models
{
    public class GridModel : IGridModel
    {
        private volatile int _version;
        private readonly IGameConfig _cfg;
        private NativeGridModel _rawData;
        public NativeGridModel GridRawData => _rawData;

        public int Version
        {
            get => _version;
            set => _version = value;
        }

        public GridModel(IGameConfig config)
        {
            _cfg = config;
            _version = 0;
            _rawData = new NativeGridModel(config.GridSize);
        }

        public bool IsObstacle(int cell) => _rawData.IsObstacle(cell);
        public int GetDirectionIndex(int cell) => _rawData.GetDirectionIndex(cell);
        public void SetObstacle(int cell, bool isObstacle) => _rawData.SetObstacle(cell, isObstacle);
        public void SetDirectionIndex(int cell, int dirIndex) => _rawData.SetDirectionIndex(cell, dirIndex);


        public void CopyTo(IGridModel target)
        {
            target.Version = Version;
            (GridRawData).CopyTo(target.GridRawData);
        }

        public NativeGridModel Clone(Allocator allocator = Allocator.TempJob,
            NativeArrayOptions opts = NativeArrayOptions.UninitializedMemory)
        {
            var copy = new NativeGridModel(_cfg.GridSize, allocator, opts);
            GridRawData.CopyTo(copy);
            return copy;
        }

        public void Dispose()
        {
            _rawData.Dispose();
        }
    }
}