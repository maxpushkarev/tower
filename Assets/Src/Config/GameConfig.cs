﻿using UnityEngine;


namespace Tower.Config
{
    [CreateAssetMenu(fileName = "TowerGameConfig", menuName = "ScriptableObjects/Tower Game Config", order = 1)]
    public class GameConfig : ScriptableObject, IGameConfig
    {
#pragma warning disable 0414
        [SerializeField]
        private int _gridSize = 16;
        [SerializeField]
        private int _frameRate = 60;
        [SerializeField]
        private int _logicFrameRate = 60;
        [SerializeField]
        private int _unitsCount = 10000;
        [SerializeField]
        private float _minUnitCellMovementTime = 0.1f;
        [SerializeField]
        private float _maxUnitCellMovementTime = 0.4f;
#pragma warning restore 0414

        public int LogicFrameRate => _logicFrameRate;
        public int UnitsCount => _unitsCount;
        public int GridSize => _gridSize;
        public float MinUnitCellMovementTime => _minUnitCellMovementTime;
        public float MaxUnitCellMovementTime => _maxUnitCellMovementTime;

        private void OnEnable()
        {
            Application.targetFrameRate = _frameRate;
        }
    }
}
