﻿namespace Tower.Config
{
    public interface IGameConfig
    {
        int GridSize { get; }
        int LogicFrameRate { get; }
        int UnitsCount { get; }
        float MinUnitCellMovementTime { get; }
        float MaxUnitCellMovementTime { get; }
    }
}