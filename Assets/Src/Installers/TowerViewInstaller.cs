using Unity.Entities;
using UnityEngine;
using Zenject;

namespace Tower.View
{
    public class TowerViewInstaller : MonoInstaller
    {
        [SerializeField]
        private GridView _gridView;
        [SerializeField]
        private UnitViewSettings _unitViewSettings;

        public override void InstallBindings()
        {
            Container.Bind<UnitViewSettings>().FromScriptableObject(_unitViewSettings).AsSingle();
            Container.Bind<IGridViewTransformCalculator>().FromInstance(_gridView).AsSingle();
            Container.QueueForInject(World.DefaultGameObjectInjectionWorld.GetExistingSystem<UnitViewBuilderSystem>());
            Container.QueueForInject(World.DefaultGameObjectInjectionWorld.GetExistingSystem<UnitViewSystem>());
        }
    }
}