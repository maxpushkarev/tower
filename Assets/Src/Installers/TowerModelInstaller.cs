﻿using Tower.Models;
using Zenject;

namespace Tower.Installers
{
    public class TowerModelInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindFactory<IGridModel, GridModelFactory>().To<GridModel>();
            Container.BindInterfacesAndSelfTo<GridModelProvider>().AsSingle();
        }
    }
}