﻿using Tower.Controllers;
using Unity.Entities;
using Zenject;

namespace Tower.Installers
{
    public class TowerControllerInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<ITargetController>().To<RandomTargetController>().AsSingle();
            Container.BindInterfacesAndSelfTo<ObstacleController>().AsSingle();

            var unitSystem = World.DefaultGameObjectInjectionWorld.GetExistingSystem<UnitSystem>();
            var gameLoop = World.DefaultGameObjectInjectionWorld.GetExistingSystem<TowerGameLoop>();

            Container.BindInterfacesTo<NativePathfindingAlgorithmExecutor>().AsSingle();
            Container.Bind<PathfindingController>().ToSelf().AsSingle();

            Container.QueueForInject(gameLoop);
            Container.QueueForInject(unitSystem);
        }
    }
}