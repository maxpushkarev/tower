using Tower.Config;
using UnityEngine;
using Zenject;

namespace Tower.Installers
{
    [CreateAssetMenu(fileName = "TowerConfigInstaller", menuName = "Installers/TowerConfigInstaller")]
    public class TowerConfigInstaller : ScriptableObjectInstaller<TowerConfigInstaller>
    {
        [SerializeReference]
        private GameConfig _cfg;

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<GameConfig>().FromInstance(_cfg).NonLazy();
        }
    }
}