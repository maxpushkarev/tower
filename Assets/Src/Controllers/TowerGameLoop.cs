﻿using System.Threading;
using Tower.Config;
using Tower.Models;
using Unity.Entities;
using Zenject;

namespace Tower.Controllers
{
    [UpdateInGroup(typeof(TowerBaseLogicSystemGroup))]
    public class TowerGameLoop : SystemBase
    {
        private ObstacleController _obstacleController;
        private PathfindingController _pathfindingController;
        private GridModelProvider _provider;

        private IGameConfig _cfg;
        private Thread _executorThread;
        
        
        private IGridModel _frontModel;
        private IGridModel _midModel;
        private IGridModel _backModel;
        private object _safeDestroy = new object();

        private volatile bool _destroyed;

        [Inject]
        private void Build(
            IGameConfig cfg, 
            ObstacleController obstacleController,
            GridModelFactory factory,
            PathfindingController pathfindingController,
            GridModelProvider provider
        )
        {
            _cfg = cfg;
            _obstacleController = obstacleController;
            _pathfindingController = pathfindingController;
            _provider = provider;

            _frontModel = factory.Create();
            _backModel = factory.Create();
            _midModel = factory.Create();

            _pathfindingController.Execute(_frontModel);
            _provider.Publish(_frontModel);

            _executorThread = new Thread(Execute);
            _executorThread.IsBackground = true;
            _executorThread.Name = "TOWER_GAME_LOOP_THREAD";
            _executorThread.Start();
        }

        private void Execute()
        {
            while (true)
            {
                if (_destroyed)
                {
                    return;
                }

                if (_obstacleController.Tick(_backModel))
                {
                    lock (_safeDestroy)
                    {
                        _pathfindingController.Execute(_backModel);
                    }

                    _backModel.Version++;

                    lock (_midModel)
                    {
                        _backModel.CopyTo(_midModel);
                        _obstacleController.MergeDiff();
                    }
                }

                var ms = 1000 / _cfg.LogicFrameRate;
                Thread.Sleep(ms);
            }
        }

        protected override void OnUpdate()
        {
            NotifyForce();

            if (_frontModel.Version != _midModel.Version)
            {
                UpdateFrontModelAndNotify();
            }
        }

        private void NotifyForce()
        {
            _obstacleController.NotifyForce(_frontModel);
        }

        private void UpdateFrontModelAndNotify()
        {
            Publish();
            _obstacleController.Notify(_frontModel);
        }

        private void Publish()
        {
            lock (_midModel)
            {
                _midModel.CopyTo(_frontModel);
                _obstacleController.PublishDiff();
                _provider.Publish(_frontModel);
            }
        }

        protected override void OnDestroy()
        {
            _destroyed = true;
            _executorThread.Abort();

            lock (_safeDestroy)
            {
                _midModel.Dispose();
                _frontModel.Dispose();
                _backModel.Dispose();
            }
        }
    }
}