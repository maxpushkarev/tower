﻿using Unity.Mathematics;
using DateTimeOffset = System.DateTimeOffset;
using Random = Unity.Mathematics.Random;

namespace Tower.Models
{
    public readonly ref struct Direction
    {
        public readonly int Dx;
        public readonly int Dy;

        public Direction(int dx, int dy)
        {
            Dx = dx;
            Dy = dy;
        }

        public void Deconstruct(out int dx, out int dy)
        {
            dx = Dx;
            dy = Dy;
        }
    }


    public static class UnitMoveUtils
    {
        public static Random GetRandom(int additionalFactor = 0) => new Random(GetRandomSeed(additionalFactor));

        private static uint GetRandomSeed(in int additionalFactor) =>
            GetRandomSeed(GetBaseRandomSeed(), additionalFactor);

        public static uint GetRandomSeed(in uint baseSeed, in int additionalFactor)
        {
            unchecked
            {
                var res = baseSeed;
                var tmp = (uint)additionalFactor;
                res ^= (tmp * tmp);
                res *= 2_147_483_647;
                return res;
            }
        }

        public static uint GetBaseRandomSeed() =>
            (uint) math.max(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 1000000007, 1);

        public static void ReadDataFromState(
            in this UnitMoveComponent move,
            in NativeGridModel.NativeReadonlyGridModel model,
            in int gridSize,
            out int toCell,
            out int fromDirectionIndex, 
            out int toDirectionIndex)
        {
            toCell = -1;
            toDirectionIndex = -1;

            fromDirectionIndex = model.GetDirectionIndex(move.From);
            if (fromDirectionIndex < 0)
            {
                return;
            }

            var fromCoord = FromLinearIndex(move.From, gridSize);

            var fromRow = fromCoord.x;
            var fromCol = fromCoord.y;

            (var dx, var dy) = GetDirectionByIndex(fromDirectionIndex);
            var toRow = fromRow + dx;
            var toCol = fromCol + dy;
            toCell = ToLinearIndex(new int2(toRow, toCol), gridSize);
            toDirectionIndex = model.GetDirectionIndex(toCell);
        }

        public static int2 FromLinearIndex(in int linear, in int size) => new int2(linear / size, linear % size);
        public static int ToLinearIndex(in int2 coord, in int size) => coord.x * size + coord.y;

        public static Direction GetDirectionByIndex(int idx)
        {
            switch (idx)
            {
                case 0:
                    return new Direction(0, 1);
                case 1:
                    return new Direction(0, -1);
                case 2:
                    return new Direction(1, 0);
                case 3:
                    return new Direction(-1, 0);
            }

            return new Direction(0, 0);
        }

        public static int GetReverseDirectionIndex(int index)
        {
            switch (index)
            {
                case 0:
                    return 1;
                case 1:
                    return 0;
                case 2:
                    return 3;
                case 3:
                    return 2;
            }

            return -1;
        }

    }

}