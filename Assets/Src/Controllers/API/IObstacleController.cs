﻿using Tower.Models;

namespace Tower.Controllers
{
    public delegate void ObstacleObserver(IReadOnlyGridModel model, int linearIndex); 

    public interface IObstacleController
    {
        void SwitchObstacle(int cell);
        event ObstacleObserver OnObstacleUpdated;
    }
}