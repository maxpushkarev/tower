﻿namespace Tower.Controllers
{
    public interface ITargetController
    {
        int TargetIndex { get; }
    }
}