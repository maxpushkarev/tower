﻿using Unity.Entities;
using Unity.Transforms;

namespace Tower.Controllers
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateAfter(typeof(FixedStepSimulationSystemGroup))]
    [UpdateBefore(typeof(TransformSystemGroup))]
    public class TowerBaseLogicSystemGroup : ComponentSystemGroup
    {
    }
}