﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using Tower.Config;
using Tower.Models;

namespace Tower.Controllers
{
    internal class ObstacleController : IObstacleController
    {
        private readonly IGameConfig _cfg;
        private readonly ITargetController _targetController;

        public ObstacleController(IGameConfig cfg, ITargetController targetController)
        {
            _cfg = cfg;
            _targetController = targetController;
        }

        private readonly ISet<int> _nonPublishedDiff = new HashSet<int>();
        private readonly ISet<int> _publishedDiff = new HashSet<int>();
        private readonly ConcurrentQueue<int> _switchCellCommands = new ConcurrentQueue<int>();
        private readonly ISet<int> _consumerSet = new HashSet<int>();
        private ICollection<ObstacleObserver> _obstacleUpdateObservers =
            new HashSet<ObstacleObserver>();

        private ISet<ObstacleObserver> _forceNotifiedObservers = new HashSet<ObstacleObserver>();

        public event ObstacleObserver OnObstacleUpdated
        {
            add
            {
                if (value != null)
                {
                    _forceNotifiedObservers.Add(value);
                    _obstacleUpdateObservers.Add(value);
                }
            }
            remove
            {
                if (value != null)
                {
                    _obstacleUpdateObservers.Remove(value);
                }
            }
        }


        public bool Tick(IGridModel model)
        {
            _consumerSet.Clear();
            int count = _switchCellCommands.Count;
            for (int i = 0; i < count; i++)
            {
                if (!_switchCellCommands.TryDequeue(out var cmd))
                {
                    break;
                }

                TryExecuteCommand(model, cmd);
            }
            return _consumerSet.Count > 0;
        }

        public void MergeDiff()
        {
            foreach (var item in _consumerSet)
            {
                if (!_nonPublishedDiff.Add(item))
                {
                    _nonPublishedDiff.Remove(item);
                }
            }
            _consumerSet.Clear();
        }

        public void PublishDiff()
        {
            _publishedDiff.Clear();
            foreach (var d in _nonPublishedDiff)
            {
                _publishedDiff.Add(d);
            }
            _nonPublishedDiff.Clear();
        }

        public void Notify(IReadOnlyGridModel model)
        {
            foreach (var d in _publishedDiff)
            {
                foreach (var observer in _obstacleUpdateObservers)
                {
                    observer.Invoke(model, d);
                }
            }
        }

        public void NotifyForce(IReadOnlyGridModel model)
        {
            foreach (var observer in _forceNotifiedObservers)
            {
                for (int i = 0; i < _cfg.GridSize * _cfg.GridSize; i++)
                {
                    if (i != _targetController.TargetIndex)
                    {
                        observer.Invoke(model, i);
                    }
                }
            }
            _forceNotifiedObservers.Clear();
        }

        private void TryExecuteCommand(IGridModel model, int cell)
        {
            if (cell < 0 || cell >= _cfg.GridSize * _cfg.GridSize || cell == _targetController.TargetIndex)
            {
                return;
            }

            SwitchInternal(model, cell);
            _consumerSet.Add(cell);
        }

        private void SwitchInternal(IGridModel model, int cell) => model.SetObstacle(cell, !model.IsObstacle(cell));
        public void SwitchObstacle(int cell) => _switchCellCommands.Enqueue(cell);
    }
}