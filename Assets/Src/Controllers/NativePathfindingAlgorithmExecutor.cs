﻿#if !UNITY_EDITOR && !UNITY_STANDALONE
#define USE_INTERNALS
#endif

using System.Runtime.InteropServices;
using Tower.Config;
using Tower.Models;

namespace Tower.Controllers
{
    public unsafe class NativePathfindingAlgorithmExecutor : IPathfindingAlgorithmExecutor
    {

#if !USE_INTERNALS
        private const string DLL_NAME = "TowerNativePathfinding";
#else
        private const string DLL_NAME = "__Internal";
#endif
        private readonly IGameConfig _config;
        private readonly ITargetController _targetController;

        public NativePathfindingAlgorithmExecutor(IGameConfig config, ITargetController targetController)
        {
            _config = config;
            _targetController = targetController;

            Init(_config.GridSize);
        }

        public void Run(IGridModel model) => RunBfs(model.GridRawData.UnsafePointer,
            _targetController.TargetIndex);


        [DllImport(DLL_NAME)]
        private static extern void RunBfs(void* data, int target);

        [DllImport(DLL_NAME)]
        private static extern void Init(int size);

        [DllImport(DLL_NAME)]
        private static extern void DisposeNative();

        public void Dispose() => DisposeNative();
    }
}