﻿using System;
using NUnit.Framework;
using Tower.Config;
using Tower.Controllers;
using Tower.Models;

public class PathfindingFixture 
{
    private class TestConfig : IGameConfig
    {
        public int GridSize => 5;
        public int LogicFrameRate => throw new NotImplementedException();
        public int UnitsCount => throw new NotImplementedException();
        public float MinUnitCellMovementTime => throw new NotImplementedException();
        public float MaxUnitCellMovementTime => throw new NotImplementedException();
    }

    private class TestTargetController : ITargetController
    {
        public int TargetIndex => 12;
    }

    [Test]
    public void ManagedAndNativePathfindingShouldBeIdentical()
    {
        var cfg = new TestConfig();
        var targetController = new TestTargetController();

        using(var managed = new ManagedPathfindingAlgorithmExecutor(cfg, targetController))
        using(var native = new NativePathfindingAlgorithmExecutor(cfg, targetController))
        using (var originModel = new GridModel(cfg))
        using (var modelForManaged = new GridModel(cfg))
        using (var modelForNative = new GridModel(cfg))
        {
            originModel.SetObstacle(1, true);
            originModel.SetObstacle(2, true);
            originModel.SetObstacle(11, true);
            originModel.SetObstacle(16, true);
            originModel.SetObstacle(24, true);
            originModel.SetObstacle(18, true);
            originModel.SetObstacle(13, true);
            originModel.SetObstacle(14, true);

            originModel.CopyTo(modelForManaged); 
            originModel.CopyTo(modelForNative);

            managed.Run(modelForManaged);
            native.Run(modelForNative);
           
            Assert.IsTrue(modelForManaged.GridRawData.Equals(modelForNative.GridRawData));
        }
    }
}
