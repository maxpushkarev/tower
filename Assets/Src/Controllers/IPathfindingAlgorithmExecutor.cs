﻿using System;
using Tower.Models;

namespace Tower.Controllers
{
    internal interface IPathfindingAlgorithmExecutor : IDisposable
    {
        void Run(IGridModel model);
    }
}