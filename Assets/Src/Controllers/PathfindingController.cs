﻿using Tower.Models;
using UnityEngine.Profiling;

namespace Tower.Controllers
{
    internal class PathfindingController
    {
        private readonly IPathfindingAlgorithmExecutor _pathfindingAlgorithmExecutor;

        public PathfindingController(IPathfindingAlgorithmExecutor pathfindingAlgorithmExecutor)
        {
            _pathfindingAlgorithmExecutor = pathfindingAlgorithmExecutor;
        }

        public void Execute(IGridModel model)
        {
            Profiler.BeginSample("PathfindingController:Execute");
            _pathfindingAlgorithmExecutor.Run(model);
            Profiler.EndSample();
        }

    }
}