﻿using System;
using System.Collections.Generic;
using Tower.Config;
using Tower.Models;
using Unity.Collections.LowLevel.Unsafe;

namespace Tower.Controllers
{
    public class ManagedPathfindingAlgorithmExecutor : IPathfindingAlgorithmExecutor
    {
        private readonly IGameConfig _config;
        private readonly ITargetController _targetController;
        private readonly bool[] _visitedVertices;
        private readonly Queue<(int cell, int dir)> _bfs;


        public ManagedPathfindingAlgorithmExecutor(IGameConfig config, ITargetController targetController)
        {
            _config = config;
            _targetController = targetController;
            _visitedVertices = new bool[_config.GridSize * _config.GridSize];
            _bfs = new Queue<(int cell, int dir)>(_config.GridSize * _config.GridSize);
        }

        public void Run(IGridModel model)
        {
            Clear(model);
            
            var n = _config.GridSize;
            _bfs.Enqueue((_targetController.TargetIndex, -1));
            _visitedVertices[_targetController.TargetIndex] = true;

            while (_bfs.Count > 0)
            {
                var curr = _bfs.Dequeue();
                model.SetDirectionIndex(curr.cell, curr.dir < 0 ? curr.dir : UnitMoveUtils.GetReverseDirectionIndex(curr.dir));

                var coord = UnitMoveUtils.FromLinearIndex(curr.cell, n);

                var x = coord.x;
                var y = coord.y;

                for (int i = 0; i < 4; i++)
                {
                    (var dx, var dy) = UnitMoveUtils.GetDirectionByIndex(i);
                    var newX = x + dx;
                    var newY = y + dy;
                    var newLin = newX * n + newY;

                    if (newX >= 0 && newY >= 0 && newX < n && newY < n && !_visitedVertices[newLin] && !model.IsObstacle(newLin))
                    {
                        _visitedVertices[newLin] = true;
                        _bfs.Enqueue((newLin, i));
                    }
                }
            }

        }

        private unsafe void Clear(IGridModel model)
        {
            fixed (void* p = &_visitedVertices[0])
            {
                UnsafeUtility.MemClear(p, Buffer.ByteLength(_visitedVertices));
            }

            for (int i = 0; i < _config.GridSize * _config.GridSize; i++)
            {
                model.SetDirectionIndex(i, -1);
            }
        }

        public void Dispose()
        {
        }
    }
}