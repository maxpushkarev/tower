﻿using Tower.Config;
using UnityEngine;

namespace Tower.Controllers
{
    public class RandomTargetController : ITargetController
    {
        public RandomTargetController(IGameConfig cfg)
        {
            TargetIndex = Random.Range(0, cfg.GridSize * cfg.GridSize);
        }

        public int TargetIndex { get; }
    }
}