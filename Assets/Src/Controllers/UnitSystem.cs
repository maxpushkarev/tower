﻿using Unity.Entities;
using Tower.Config;
using Tower.Models;
using Unity.Collections;
using UnityEngine.Profiling;
using Zenject;
using Random = Unity.Mathematics.Random;

namespace Tower.Controllers
{
    [UpdateAfter(typeof(TowerGameLoop))]
    [UpdateInGroup(typeof(TowerBaseLogicSystemGroup))]
    internal class UnitSystem : SystemBase
    {
        private IGameConfig _cfg;
        private IReadonlyGridModelProvider _provider;
        private ITargetController _targetController;


        [Inject]
        private void SetDeps(IGameConfig cfg, IReadonlyGridModelProvider provider, ITargetController targetController)
        {
            _cfg = cfg;
            _provider = provider;
            _targetController = targetController;

            var count = _cfg.UnitsCount;
            var cellsCount = _cfg.GridSize * _cfg.GridSize;
            var rnd = UnitMoveUtils.GetRandom();
            var unitArchetype = EntityManager.CreateArchetype(typeof(UnitMoveComponent));

            using (var buffer = new EntityCommandBuffer(Allocator.Temp))
            {
                for (int i = 0; i < count; i++)
                {
                    var unitEntity = buffer.CreateEntity(unitArchetype);
                    var unitMoveComponent = new UnitMoveComponent();
                    SpawnUnitAtRandomCell(ref unitMoveComponent, in cellsCount, rnd.NextFloat(_cfg.MinUnitCellMovementTime, _cfg.MaxUnitCellMovementTime), ref rnd);
                    buffer.SetComponent(unitEntity, unitMoveComponent);
                }
                buffer.Playback(EntityManager);
            }
        }

        private static void SpawnUnitAtRandomCell(ref UnitMoveComponent unitMove, in int cellsCount, in float movementTime, ref Random rnd)
        {
            unitMove.CurrentTime = movementTime;
            unitMove.InitialTime = movementTime;
            unitMove.From = rnd.NextInt(0, cellsCount);
        }
        
        protected override void OnUpdate()
        {
            Profiler.BeginSample("UnitSystem::OnUpdate");

            var targetIndex = _targetController.TargetIndex;
            var dt = Time.DeltaTime;
            var size = _cfg.GridSize;
            var cellsCount = size * size;

            var state = _provider.Model.Clone();
            var minMoveTime = _cfg.MinUnitCellMovementTime;
            var maxMoveTime = _cfg.MaxUnitCellMovementTime;

            var baseSeed = UnitMoveUtils.GetBaseRandomSeed();

            var moveAllUnitsJob = Entities
                .WithAll<UnitMoveComponent>()
                .ForEach(
                    (Entity entity, int entityInQueryIndex, ref UnitMoveComponent movement) =>
                    {
                        var readOnlyState = state.AsReadOnly();
                        var rnd = new Random(UnitMoveUtils.GetRandomSeed(baseSeed, entityInQueryIndex));
                        movement.ReadDataFromState(in readOnlyState, in size,
                            out var toCell, out var fromDirIndex, out var toDirIndex);

                        var movementTime = rnd.NextFloat(minMoveTime, maxMoveTime);

                        if (movement.From == targetIndex ||
                            toCell == targetIndex ||
                            fromDirIndex < 0 ||
                            toDirIndex < 0 ||
                            toCell < 0)
                        {
                            SpawnUnitAtRandomCell(ref movement, in cellsCount,
                                in movementTime, ref rnd);
                            return;
                        }


                        movement.CurrentTime -= dt;

                        if (movement.CurrentTime <= 0)
                        {
                            movement.From = toCell;
                            movement.CurrentTime = movementTime;
                            movement.InitialTime = movementTime;
                        }

                    })
                    .WithDisposeOnCompletion(state)
                    .ScheduleParallel(Dependency);


                Dependency = moveAllUnitsJob;

                Profiler.EndSample();
        }

        protected override void OnDestroy()
        {
            
        }


    }
}