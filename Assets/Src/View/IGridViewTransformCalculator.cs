﻿using Unity.Mathematics;

namespace Tower.View
{
    public interface IGridViewTransformCalculator
    {
        void CalculateWorldSpaceData(
            out float size, 
            out float3 origin,
            out float cellSize,
            out float unitSize);
    }
}