﻿using Tower.Config;
using Tower.Controllers;
using Tower.Models;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Zenject;
using Random = Unity.Mathematics.Random;

namespace Tower.View
{
    [UpdateInGroup(typeof(TransformSystemGroup))]
    public class UnitViewSystem : SystemBase
    {
        private IReadonlyGridModelProvider _provider;
        private IGridViewTransformCalculator _transformCalculator;
        private IGameConfig _cfg;
        private ITargetController _targetController;

        [Inject]
        private void SetDeps(
            IReadonlyGridModelProvider provider, 
            IGridViewTransformCalculator gridTransformCalculator,
            IGameConfig config,
            ITargetController targetController)
        {
            _provider = provider;
            _transformCalculator = gridTransformCalculator;
            _cfg = config;
            _targetController = targetController;
        }

        protected override void OnUpdate()
        {
            _transformCalculator.CalculateWorldSpaceData(out var gridViewSize, out var gridOrigin, out var cellSize, out var unitSize);
            var state = _provider.Model.Clone();
            var size = _cfg.GridSize;
            var targetIndex = _targetController.TargetIndex;
            var fullCellSize = gridViewSize / size;
            var baseSeed = UnitMoveUtils.GetBaseRandomSeed();

            Entities
                .WithBurst(FloatMode.Fast, FloatPrecision.Low)
                .ForEach(
                    (int entityInQueryIndex, 
                        ref LocalToWorld localToWorld,
                        ref UnitSmoothingViewComponent smoothMove,
                        in UnitMoveComponent unitMove) =>
                    {
                        var rnd = new Random(UnitMoveUtils.GetRandomSeed(baseSeed, entityInQueryIndex));
                        var readOnlyState = state.AsReadOnly();
                        unitMove.ReadDataFromState(in readOnlyState, in size,
                            out var toCell, out var fromDirIndex, out var toDirIndex);

                        float4x4 l2w = float4x4.identity;

                        if (unitMove.From == targetIndex ||
                            toCell == targetIndex ||
                            fromDirIndex < 0 ||
                            toDirIndex < 0 ||
                            toCell < 0)
                        {
                            //TODO: think about another way to hide
                            //switch layer of RenderMesh component is not a good idea as it's shared component
                            var c3 = l2w.c3;
                            c3[2] = -1000;
                            l2w.c3 = c3;
                            localToWorld.Value = l2w;
                            return;
                        }


                        var fromCoord = UnitMoveUtils.FromLinearIndex(unitMove.From, size);
                        var toCoord = UnitMoveUtils.FromLinearIndex(toCell, size);

                        fromCoord.xy = fromCoord.yx;
                        toCoord.xy = toCoord.yx;

                        if (math.abs(unitMove.InitialTime - unitMove.CurrentTime) <= 0.001f)
                        {
                            smoothMove.RndFrom = smoothMove.RndTo;
                            smoothMove.RndTo = rnd.NextFloat2(new float2(1.0f, 1.0f));
                        }

                        float3 from = CalculateWorldPosition(gridOrigin, fullCellSize, fromCoord, smoothMove.RndFrom,
                            cellSize);
                        float3 to = CalculateWorldPosition(gridOrigin, fullCellSize, toCoord, smoothMove.RndTo,
                            cellSize);

                        float3 p = math.lerp(to, from, math.clamp(unitMove.CurrentTime / unitMove.InitialTime, 0,1));

                        l2w.c0 = new float4(unitSize, 0, 0, 0);
                        l2w.c1 = new float4(0, unitSize, 0, 0);
                        l2w.c2 = new float4(0, 0, unitSize, 0);
                        l2w.c3 = new float4(p.x, p.y, p.z, 1);

                        localToWorld.Value = l2w;
                    })
                .WithDisposeOnCompletion(state)
                .ScheduleParallel();
        }

        private static float3 CalculateWorldPosition(in float3 gridOrigin, in float fullCellSize, in int2 coords,
            in float2 rndFactors, in float cellSize)
        {
            float3 res = gridOrigin;
            res.xy += new float2(fullCellSize, fullCellSize) * coords;
            res.xy += fullCellSize * 0.5f; //center Of cell
            res.xy -= cellSize * 0.5f; //bottomLeft of cell
            res.xy += rndFactors * cellSize;
            return res;
        }
    }
}