﻿using Unity.Entities;
using Unity.Mathematics;

namespace Tower.View
{
    public struct UnitSmoothingViewComponent : IComponentData
    {
        public float2 RndFrom;
        public float2 RndTo;
    }
}