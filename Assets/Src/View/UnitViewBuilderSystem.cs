﻿using Tower.Controllers;
using Tower.Models;
using Unity.Collections;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;
using Unity.Mathematics;
using Zenject;

namespace Tower.View
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateAfter(typeof(TowerBaseLogicSystemGroup))]
    [UpdateBefore(typeof(TransformSystemGroup))]
    public class UnitViewBuilderSystem : SystemBase
    {
        private int _layer;
        private UnitViewSettings _unitViewSettings;

        [Inject]
        private void SetDeps(UnitViewSettings unitViewSettings)
        {
            _unitViewSettings = unitViewSettings;
        }

        protected override void OnCreate()
        {
            _layer = LayerMask.NameToLayer("Default");
        }

        protected override void OnUpdate()
        {
            using (var commandBuffer = new EntityCommandBuffer(Allocator.Temp, PlaybackPolicy.SinglePlayback))
            {
                var layer = _layer;
                var mat = _unitViewSettings.Material;
                var mesh = _unitViewSettings.Mesh;

                Entities
                    .WithoutBurst()
                    .WithAll<UnitMoveComponent>()
                    .WithNone<LocalToWorld, RenderMesh>()
                    .ForEach((Entity e) =>
                    {
                       commandBuffer.AddComponent<LocalToWorld>(e);
                       commandBuffer.AddSharedComponent(e, new RenderMesh()
                       {
                           castShadows = ShadowCastingMode.Off,
                           layer = layer,
                           material = mat,
                           mesh = mesh,
                           needMotionVectorPass = false,
                           receiveShadows = false,
                           subMesh = 0
                       });
                       commandBuffer.AddComponent(e, new RenderBounds()
                       {
                             Value  = new AABB()
                             {
                                 Center = float3.zero,
                                 Extents = new float3(0.5f, 0.5f, 0.5f)
                             }
                       });
                       commandBuffer.AddComponent<UnitSmoothingViewComponent>(e);

                    }).Run();

                commandBuffer.Playback(EntityManager);
            }

        }
    }
}