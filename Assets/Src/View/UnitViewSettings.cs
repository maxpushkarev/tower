﻿using UnityEngine;

namespace Tower.View
{
    [CreateAssetMenu(fileName = "UnitViewSettings", menuName = "ScriptableObjects/Unit view settings", order = 1)]
    public class UnitViewSettings : ScriptableObject
    {
        [SerializeField]
        private Mesh _mesh;
        [SerializeField]
        private Material _material;

        public Mesh Mesh => _mesh;
        public Material Material => _material;
    }
}