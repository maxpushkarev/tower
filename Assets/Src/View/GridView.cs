﻿using Tower.Config;
using Tower.Controllers;
using Tower.Models;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Tower.View
{
    public class GridView : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler, IGridViewTransformCalculator
    {
        [Header("Elements")] 
        [SerializeField] 
        private Canvas _canvas;
        [SerializeField]
        private RectTransform _gridRectTransform;
        [SerializeField]
        private Image _gridImage;

        [Header("View settings")]
        [SerializeField]
        [Range(0.0f, 1.0f)]
        private float _cellWithoutBordersSizeFactor = 0.8f;
        [SerializeField]
        [Range(0.0f, 1.0f)]
        private float _unitByCellSizeFactor = 0.1f;
        [SerializeField]
        private Color _obstacleColor;
        [SerializeField]
        private Color _freeColor;
        [SerializeField]
        private Color _targetColor;

        private IGameConfig _cfg;
        private IObstacleController _gridController;
        private ITargetController _targetController;
        private Material _mat;

        private int? _lastCell;
        private Vector3[] _corners = new Vector3[4];

        private bool _mapDirty;
        private Texture2D _map;

        [Inject]
        private void SetDependencies(
            IGameConfig cfg,
            IObstacleController controller,
            ITargetController targetController)
        {
            _cfg = cfg;
            _gridController = controller;
            _targetController = targetController;
        }

        private void Awake()
        {
            CreateTableView();
        }

        private Texture2D CreateTex()
        {
            var res = new Texture2D(_cfg.GridSize, _cfg.GridSize, TextureFormat.R8, false);
            res.name = "GridViewMap";
            res.filterMode = FilterMode.Point;
            res.anisoLevel = 0;
            return res;
        }

        private void OnDestroy()
        {
            Destroy(_map); 
            Destroy(_mat);
        }

        private void OnEnable() => _gridController.OnObstacleUpdated += OnCellObstacleUpdated;
        private void OnDisable() => _gridController.OnObstacleUpdated -= OnCellObstacleUpdated;

        private void OnCellObstacleUpdated(IReadOnlyGridModel model, int cell)
        {
            _mapDirty = true;
            int size = _cfg.GridSize;
            var coord = UnitMoveUtils.FromLinearIndex(cell, size);
            _map.SetPixel(coord.x, coord.y, model.IsObstacle(cell) ? Color.white : Color.white * 0.5f);
        }

        private void LateUpdate()
        {
            if (_mapDirty)
            {
                _map.Apply();
                _mapDirty = false;
            }
        }

        private void CreateTableView()
        {
            _mat = Instantiate(_gridImage.material);
            _mat.SetFloat("_ActualCellSizeFactor", _cellWithoutBordersSizeFactor);
            _mat.SetInt("_GridSize", _cfg.GridSize);
            _mat.SetColor("_TargetColor", _targetColor);
            _mat.SetColor("_FreeColor", _freeColor);
            _mat.SetColor("_ObstacleColor", _obstacleColor);
            _map = CreateTex();
            _mat.SetTexture("_Map", _map);
            _gridImage.material = _mat;

            int size = _cfg.GridSize;
            var targetCoord = UnitMoveUtils.FromLinearIndex(_targetController.TargetIndex, size);
            _mapDirty = true;
            _map.SetPixel(targetCoord.x, targetCoord.y, Color.black);
        }

        private int2? GetRelativeCoord(Vector2 screenPos)
        {
            var cam = _canvas.worldCamera;
            Vector2 centerGrid = RectTransformUtility.WorldToScreenPoint(cam, _gridRectTransform.position);
            _gridRectTransform.GetWorldCorners(_corners);

            _corners[2] = RectTransformUtility.WorldToScreenPoint(cam, _corners[2]);
            _corners[0] = RectTransformUtility.WorldToScreenPoint(cam, _corners[0]);

            float size = (_corners[2].x - _corners[0].x);
            float halfSize = size * 0.5f;
            Vector2 bottomLeft = centerGrid - new Vector2(halfSize, halfSize);
            Vector2 rel = screenPos - bottomLeft;
            var fullSize = size / _cfg.GridSize;
            var coord = new int2(Mathf.FloorToInt(rel.y / fullSize), Mathf.FloorToInt(rel.x / fullSize));

            if (coord.x >= 0 && coord.y >= 0 && coord.x < _cfg.GridSize && coord.y < _cfg.GridSize)
            {
                return coord;
            }

            return null;
        }

        public void CalculateWorldSpaceData(out float size,
            out float3 origin,
            out float cellSize,
            out float unitSize)
        {
            size = CalculateWorldSpaceSize();
            var halfSize = 0.5f * size;
            origin = _gridRectTransform.position - new Vector3(halfSize, halfSize, 0);
            cellSize = (size / _cfg.GridSize) * _cellWithoutBordersSizeFactor;
            unitSize = cellSize * _unitByCellSizeFactor;
        }

        private float CalculateWorldSpaceSize()
        {
            _gridRectTransform.GetWorldCorners(_corners);
            return (_corners[2].x - _corners[0].x);
        }


        public void OnPointerDown(PointerEventData eventData) => HandleCellInput(eventData.position);
        public void OnDrag(PointerEventData eventData) => HandleCellInput(eventData.position);
        public void OnPointerUp(PointerEventData eventData) => _lastCell = null;

        private void HandleCellInput(Vector2 position)
        {
            var coord = GetRelativeCoord(position);
            int? lin = null;
            int size = _cfg.GridSize;
            if (coord != null)
            {
                lin = UnitMoveUtils.ToLinearIndex(coord.Value, in size);
            }

            if (lin != _lastCell)
            {
                if (lin.HasValue)
                {
                    ProcessCell(lin.Value);
                }

                _lastCell = lin;
            }
        }

        private void ProcessCell(int linearCoord) => _gridController.SwitchObstacle(linearCoord);

    }
}