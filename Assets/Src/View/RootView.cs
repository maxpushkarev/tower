﻿using UnityEngine;
using UnityEngine.UI;

namespace Tower.View
{
    [RequireComponent(typeof(CanvasScaler))]
    public class RootView : MonoBehaviour
    {
        [SerializeField]
        private CanvasScaler _canvasScaler;

        private Vector2Int? _resolution;

        private void Awake()
        {
            CheckResolution();
        }

        private void Update()
        {
            CheckResolution();
        }

        private void CheckResolution()
        {
            var resolution = new Vector2Int(Screen.width, Screen.height);
            if (
                !_resolution.HasValue || 
                _resolution.Value.x != resolution.x ||
                _resolution.Value.y != resolution.y)
            {
                _canvasScaler.matchWidthOrHeight = (resolution.x <= resolution.y) ? 0 : 1;
                _resolution = resolution;
            }
        }

        
    }
}
